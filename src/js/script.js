var flag = true

var NR = {
	header: function() {
		//헤더 스크롤
		var scrollPos = 0;
		var scrollDir = "dn";
		$(window).on("scroll", function() {
			if($("body").hasClass("popup-open")) return;
			var getScrollTop = $(window).scrollTop();
			var getScrollBottom = getScrollTop + $(window).height();
			var getHeight = $(document).height();
			if(scrollPos !== getScrollTop) {
				if(scrollPos > getScrollTop) scrollDir = "up";
				else if(scrollPos < getScrollTop) scrollDir = "dn";
			}
			
			if(scrollDir !== "up" && getScrollTop > 30 && getScrollBottom + 100 < getHeight) {
				$("body").addClass("scroll");
			}
			else {
				$("body").removeClass("scroll");
			}
			
			scrollPos = getScrollTop;
		});
		$("#btn-page-top").on("click", function() {
			$("html,body").animate({ scrollTop:0 }, 200);
			return false;
		});
		
		//gnb 슬라이드
		if($("header .gnb-slide").length > 0) {
			var gnbActive = $("header .gnb-slide .btn-menu.active");
			var gnbIdx = 0;
			if(gnbActive.length > 0) {
				var gnbParent = gnbActive.parent();
				var gnbPos = gnbParent.offset().left + gnbParent.width();
				if($(window).width() < gnbPos) {
					gnbIdx = gnbParent.index();
				}
			}
			var gnbMenu = $("header .gnb-slide .swiper-slide");
			var totalWidth = 0;
			var windowWidth = $(window).width();
			gnbMenu.each(function(){
				totalWidth = totalWidth + $(this).outerWidth();
			});
			if( windowWidth < totalWidth ) {
				var swiper = new Swiper("header .gnb-slide", {
					slidesPerView: "auto",
					freeMode: true,
					slidesOffsetBefore: 10,
					slidesOffsetAfter: 10
				});
				swiper.slideTo(gnbIdx, 0);
			}
		}

		if($(".faq .faq-title-slide").length > 0) {
			var gnbActive = $(".faq .faq-title-slide .btn-menu.active");
			var gnbIdx = 0;
			if(gnbActive.length > 0) {
				var gnbParent = gnbActive.parent();
				var gnbPos = gnbParent.offset().left + gnbParent.width();
				if($(window).width() < gnbPos) {
					gnbIdx = gnbParent.index();
				}
			}
			var swiper = new Swiper(".faq .faq-title-slide", {
				slidesPerView: "auto",
		        freeMode: true
	      	});
	      	swiper.slideTo(gnbIdx, 0);
		}

		//카테고리 메뉴
      	$("header .btn-category").on("click", function() {
      		if($("header").hasClass("category-open")) {
      			NR.popupClose("header");
	      		$("header").removeClass("category-open");
      		}
      		else {
      			NR.popupOpen("header");
	      		$("header").addClass("category-open");
      		}
      		return false;
      	});
      	$("header .category-menu").on("click", function(e) {
      		if($(e.target).hasClass("category-menu")) {
      			NR.popupClose("header");
      			$("header").removeClass("category-open");
      			return false;
      		}
      	});
      	
      	//헤더 전체 메뉴
      	$("#btn-total-menu").on("click", function() {
      		NR.popupOpen("#header-total-menu");
      		$("#header-total-menu").stop(true,true).fadeIn(200);
      		setTimeout(function() {
      			$("#header-total-menu").addClass("active");
      		}, 100);
      		return false;
      	});
      	$("#header-total-menu .btn-close").on("click", function() {
      		NR.popupClose("#header-total-menu");
      		$("#header-total-menu").removeClass("active").stop(true,true).fadeOut(200);
      		$("#header-total-menu .gnb-list .btn-gnb.active").removeClass("active").next().hide();
      		return false;
      	});
      	$("#header-total-menu .gnb-list .btn-gnb.expand").on("click", function() {
      		if($(this).hasClass("active")) {
      			$(this).removeClass("active").next().stop(true,true).slideUp(200);
      		}
      		else {
      			$("#header-total-menu .gnb-list .btn-gnb.active").removeClass("active").next().stop(true,true).slideUp(200);
      			$(this).addClass("active").next().stop(true,true).slideDown(200);
      		}
      		return false;
      	});
	},
	
	footer: function() {
		//플로팅 메뉴
		$("#float-menu .btn-menu.float").on("click", function() {
			$("#float-menu").toggleClass("active");
			return false;
		});
		$("#float-menu .btn-menu.top").on("click", function() {
			$("html, body").stop(true, true).animate({ scrollTop:0 }, 200);
			return false;
		});
		$("#float-menu .float-mask").on("click", function(e) {
			if($(e.target).hasClass("float-mask")) {
				$("#float-menu").removeClass("active");
				return false;
			}
		});
		if($("footer").length == 0 && $(".footer-menu").length > 0) {
			$(".content").eq(0).addClass("bottom");
		}
	},
	
	notibar: function() {
		//탑 노티 바
		$("header .header-notibar .btn-close").on("click", function() {
			// var notibar = $("header .header-notibar").animate({ height:0 }, 200);
			// $("body.notibar").animate({ paddingTop:0 }, 200, function() {
			// 	$(this).removeClass("notibar");
			// 	notibar.remove();
			// });
			$('body.notibar').removeClass("notibar");
			$('.header-notibar').remove();
			return false;
		});
		if( $('body').hasClass('notibar') == true ){
			$(window).on('scroll', function(){
				var top = $(window).scrollTop();
				if( top > 100 && $('.header-notibar').length > 0 ){
					$('body').removeClass('notibar');
				} else if( top < 100 && $('.header-notibar').length > 0 && !$('body').hasClass('popup-open') ) {
					$('body').addClass('notibar');
				}
			})
		}
	},

	newArrival: function(){
		//스크롤 에니메이션
		var naAnimate = $(".na-animate");
		var scrollPrev = 0;
		var scrollDir = "dn";
		$(window).on("scroll", function() {
			var getTop = $(window).scrollTop();
			var getBottom = getTop + $(window).height();
			var getOffset = 200;
			if(getTop !== scrollPrev) {
				if(scrollPrev < getTop) scrollDir = "dn";
				else if(scrollPrev > getTop) scrollDir = "up";
			}
			scrollPrev = getTop;
			
			naAnimate.each(function() {
				var itemTop = $(this).offset().top;
				var itemBottom = itemTop + $(this).height();
				if(scrollDir == "dn") {
					if(itemTop < getBottom - getOffset) {
						$(this).addClass("animated");
					}
				}
				else {
					if(itemTop > getBottom - getOffset) {
						// $(this).removeClass("animated");
					}
				}
			});
		}).trigger("scroll");
	},
	
	tab: function() {
		$(".content-tab .btn-tab").on("click", function() {
			$(this).addClass("active").siblings().removeClass("active");
			return false;
		});
		
		$(".item-tab:not(.tab-page) .btn-tab").on("click", function() {
			$(this).addClass("active").siblings().removeClass("active");
			return false;
		});

		$(".item-tab.tab-page .btn-tab").on("click", function() {
			flag = false
			setTimeout(function(){
				flag = true;
			},1000)
			var getIdx = $(this).addClass("active").index();
			$(this).siblings().removeClass("active");
			var getTop = $(".tab-page-area").eq(getIdx).offset().top - $("header").height() - $(".item-tab").height() ;
			$("html,body").animate({ scrollTop: getTop }, 200);
			return false;
		});
	},
	stickyTab: function(){
		if($('.sticky-tab').length > 0){
			var pagesOffset = [];
			function getOffest(){
				var arr = [];
				$('.tab-page-area').each(function(i, e){
					arr.push(parseInt($(e).offset().top));
				});
				pagesOffset = arr;
				//console.log('pagesOffset: '+ pagesOffset);
			}
			getOffest();

			$(window).on("scroll", function() {
				if(flag == true) {
					var gap = $("header").height() + $(".item-tab").height();
					var getTop = $(window).scrollTop() + gap;
					//console.log(getTop);
					if( getTop < pagesOffset[0] ){
						$('.tab-page .btn-tab').eq(0).addClass('active').siblings().removeClass('active');
					} else if( pagesOffset[0] < getTop  && getTop < pagesOffset[1] ){
						$('.tab-page .btn-tab').eq(0).addClass('active').siblings().removeClass('active');
					} else if( pagesOffset[1] < getTop && getTop < pagesOffset[2] ){
						$('.tab-page .btn-tab').eq(1).addClass('active').siblings().removeClass('active');
					} else if( pagesOffset[2] < getTop && getTop < pagesOffset[3] ){
						$('.tab-page .btn-tab').eq(2).addClass('active').siblings().removeClass('active');
					} else {
						$('.tab-page .btn-tab').eq(3).addClass('active').siblings().removeClass('active');
					} 
					//아코디언들이 있어서 offset값 다시 받기
					getOffest();
				}
			})

		}
	},
	
	select: function() {
		return;
		
		$(".select-drop-down").each(function() {
			var getTarget = $(this);
			var getSelect = $(this).find("select");
			var getBtn = $(this).find(".btn-select");
			var getText = getSelect.find("option:selected").text();
			var getValue = getSelect.val();
			getBtn.removeClass("placeholder").html(getText);
			if(getValue == "") getBtn.addClass("placeholder");
			var getList = [];
			getSelect.find("option").each(function() {
				if(this.value !== "") {
					getList.push('<a href="#" data-value="' + this.value + '" class="btn-menu">' + $(this).text() + '</a>');
				}
			});
			$(this).append('<div class="select-drop-list">' + getList.join("") + '</div>');
			getBtn.on("click", function() {
				getTarget.toggleClass("active");
				if(getTarget.hasClass("active")) {
					$("body").one("click.selectdropdown", function(e) {
						if($(e.target).closest(".select-drop-down").length == 0) {
							$(".select-drop-down.active").removeClass("active");
						}
					});
				}
				return false;
			});
			getTarget.find(".select-drop-list .btn-menu").on("click", function() {
				var setText = $(this).text();
				var setValue = $(this).data("value");
				getSelect.val(setValue);
				getBtn.removeClass("placeholder").html(setText);
				$(this).parent().find(".btn-menu").removeClass("active");
				$(this).addClass("active");
				getTarget.removeClass("active");
				$("body").off("click.selectdropdown");
				if(getTarget.hasClass("select-url") && setValue !== "") {
					location.href = setValue;
				}
				return false;
			});
		});
	},
	
	file: function() {
		//첨부파일 선택
		$(".file-box .input-file").on("change", function() {
			var getFile = this.files[0];
			var getInput = $(this).parent().find(".input-text").val("");
			if(getFile !== undefined && getFile.name !== undefined && getFile.name !== "") {
				getInput.val(getFile.name);
			}
		});
	},
	
	form: function() {
		//키입력수 제한
		$(".input-text.input-limit").on("keyup", function() {
			var getText = this.value;
			var getCount = $($(this).data("for"));
			var getMaxlength = $(this).attr("maxlength");
			if(getCount.length > 0 && getMaxlength !== undefined) {
				var getLength = getText.length;
				if(getLength > getMaxlength) {
					getCount.html(getMaxlength);
					this.value = this.value.substr(0, getMaxlength);
				}
				else {
					getCount.html(getText.length);
				}
			}
		});
		
		//전체 선택
		$(".check-all[data-target]").each(function() {
			var getCheckAll = $(this);
			var getCheckCount = $(this).closest(".check-box").find(".count");
			var getTarget = $($(this).data("target"));
			if(getTarget.length > 0) {
				var getCheckList = getTarget.find(".check");
				if(getCheckList.length > 0) {
					getCheckCount.html("<b>0</b>/" + getCheckList.length);
					getCheckList.on("change", function() {
						getCheckCount.find("b").html(getTarget.find(".check:checked").length);
						getCheckAll.prop("checked", getTarget.find(".check:not(:checked)").length == 0);
					});
				}
			}
		}).on("change", function() {
			var getCheckAll = $(this);
			var getCheckCount = $(this).closest(".check-box").find(".count");
			var getTarget = $($(this).data("target"));
			if(getTarget.length > 0) {
				getTarget.find(".check").prop("checked", this.checked);
				getCheckCount.find("b").html(getTarget.find(".check:checked").length);
			}
		});
		
		$("#btn-check-order-all").on("click", function() {
			$("#check-order-list .check").prop("checked", true);
			return false;
		});
	},
	
	popupOpen: function(target) {
		if(target == undefined) return;
		var getScrollTop = $(window).scrollTop();
		/***** s : DEV *****/
		if(!$(target).hasClass("active")) {
			$("body").addClass("popup-open").css("top", -getScrollTop + "px").data("scrollTop", getScrollTop);
		}
		/***** e : DEV *****/
		if($(target).hasClass("popup-window")) {
			$(target).addClass("active").fadeIn(200).find(".popup-content").scrollTop(0);
		}
	},
	popupClose: function(target) {
		if(target == undefined) return;
		var getScrollTop = $("body").removeClass("popup-open").removeAttr("top").data("scrollTop");
		if(getScrollTop !== undefined && !isNaN(getScrollTop)) {
			window.scrollTo({ top:getScrollTop, behavior:"instant" });
		}
		if($(target).hasClass("popup-window")) {
			$(target).closest(".popup-window").removeClass("active").fadeOut(200);
		}
	},
	
	popup: function() {
		$(".popup-window .popup-header .btn-close, .popup-window .popup-footer .btn-cancel").on("click", function(e) {
			NR.popupClose($(this).closest(".popup-window"));
			return false;
		});
		$(".popup-window").on("click", function(e) {
			if($(e.target).hasClass("popup-window")) {
				NR.popupClose(this);
				return false;
			}
		});
		
		//필터 팝업
		$("#popup-review-detail .btn-filter").on("click", function(e) {
			NR.popupOpen($("#popup-item-filter"));
			return false;
		});
		
		//팝업 필터 팝업
		$(".item-filter .btn-filter").on("click", function(e) {
			NR.popupOpen($("#popup-item-filter"));
			return false;
		});

		//고객센터 메뉴
		$('header .btn-drop-menu').on('click', function(e){
			NR.popupOpen(".cs-menu-wrap");
			$('.cs-menu-wrap').addClass('open');
			return false;
		});
		
		$('.cs-menu-wrap .btn-close').on('click', function(e){
			NR.popupClose(".cs-menu-wrap");
			$('.cs-menu-wrap').removeClass('open');
			return false;
		});
		
		//상품상세 리뷰 상세 팝업
		$(".pd-content-area .btn-review-write").on("click", function() {
			NR.popupOpen("#popup-review-write");
			return false;
		});
		
		//상품상세 필터 팝업
		$(".pd-content-area .btn-filter").on("click", function(e) {
			NR.popupOpen($("#popup-item-filter"));
			return false;
		});
		
		//상품상세 필터 팝업
		$(".pd-info-area .btn-share").on("click", function(e) {
			NR.popupOpen($("#popup-item-share"));
			return false;
		});
		
		//이미지 상세 팝업
		$(".image-detail-popup img, .image-detail-popup .btn-link").on("click", function() {
			//TODO 이미지 상세 리스트 가져오기 html 에 data 명시 또는 ajax 로 데이터 가져오기 등 우선 임의로 data-detail 에 이미지 리스트 명시함
			var detailImg = $(this).closest(".image-detail-popup").data("detail");
			if(detailImg !== undefined && detailImg !== "") {
				var detailIdx = 0;
				if($(this).closest(".btn-link").length == 0 && $(this).closest("li").length > 0) {
					detailIdx = $(this).closest(".image-detail-popup").find("li").index($(this).closest("li"));
				}
				
				NR.popupOpen($("#popup-image-detail"));
				var detailPopup = $("#popup-image-detail").find(".slide-wrap");
			
				detailImg = detailImg.split(",");
				var slideList = [];
				$.each(detailImg, function(key, value) {
					slideList.push('<div class="swiper-slide"><img src="' + value + '" alt=""></div>');
				});
				detailPopup.html('<div class="image-slide swiper-container"><div class="swiper-wrapper">' + slideList.join("") + '</div></div>');
				new Swiper(detailPopup.find(".image-slide").get(0), {
					initialSlide: detailIdx,
					centeredSlides: true,
		        	slidesPerView: 1.25,
		        	spaceBetween: 15
		      	});
			}
			
			return false;
		});

		//통합검색 팝업
		// $('.btn-menu').on('click', function(e){
			// if( $(this).find('.icon-search').length > 0 ){
				// NR.popupOpen($("#popup-search-total"));
			// }
			// return false;
		// });
		// 영상팝업
		if($('.video-modal').length > 0) {
			$(document).ready(function(){
				$('.video-modal').addClass("on");
			});
			
      		setTimeout(function() {
      			$("#header-total-menu").addClass("active");
      		}, 100);
		}
		$('.video-modal .btn-close, .video-modal .btn-no-more').click(function(e){
			e.preventDefault();
			$(this).parents('.video-modal').removeClass('on');
		})
	},
	
	faq: function() {
		$(".bbs-faq-list .faq-title").on("click", function() {
			if($(this).hasClass("active")) {
				$(this).removeClass("active").next().stop(true,true).slideUp(200);
			}
			else {
				$(".bbs-faq-list .faq-title.active").removeClass("active").next().stop(true,true).slideUp(200);
				$(this).addClass("active").next().stop(true,true).slideDown(200);
			}
			return false;
		});
		if($('.faq .list-more-wrap').length > 0) {
			$(".faq .list-more-wrap + .btn-list-more").on("click", function() {
				$(this).toggleClass("on");
				if($(this).hasClass("on")) {
					$(this).html('<a href="#" class="btn-list-more">닫기<span class="icon icon-list-more"></span></a>');
				}else {
					$(this).html('<a href="#" class="btn-list-more">더보기<span class="icon icon-list-more"></span></a>');
				}
				$(this).siblings(".list-more-wrap").toggleClass("on").find(".faq-title").removeClass("active").next().stop(true,true).slideUp(200)
				return false;
			});
		}
	},
	
	review: function() {
		//도움 버튼
		$(".review-view-area .btn-review-helpful").on("click", function() {
			$(this).toggleClass("on");
			return false;
		});
		
		//더보기
		$(".review-view-area .btn-list-more").on("click", function() {
			$(this).closest(".review-view-area").toggleClass("active");
			return false;
		});
		
		//리뷰 점수
		$("#popup-review-write").on("click", ".point-box .icon", function() {
			$(this).parent().find(".icon").removeClass("on");
			$(this).addClass('on').prevAll(this).addClass("on");
			return false;
		});
		//리뷰 업로드
		$("#popup-review-write").on("change", ".write-picture-box .input-file", function() {
			var getFile = this.files[0];
			$(this).parent().removeClass("upload").find(".img").css("background-image", "none");
			if(getFile !== undefined && getFile.name !== undefined && getFile.name !== "") {
				var getImg = $(this).parent().addClass("upload").find(".img");
				var reader = new FileReader();
				reader.onload = function() {
				  	getImg.css("background-image","url('" + reader.result + "')");
				}
				reader.readAsDataURL(getFile);
			}
		}).on("click", ".write-picture-box .btn-close", function() {
			$(this).parent().removeClass("upload").find(".img").css("background-image", "none").end().find(".input-file").val("");
			return false;
		});
	},
	
	slider: function() {
		//메인 카테고리 비주얼 슬라이드
		if($(".category-visual-area .category-slide").length > 0) {
			var newArSlideBottom = new Swiper(".category-visual-area .category-slide", {
				loop: true,
				slidesPerView: 1,
				watchOverflow:true,
	        	pagination: {
	        		el: ".category-visual-area .slider-page-bar",
					type: "progressbar"
	        	},
				onSlideChangeStart: function (swiper) {
					$('.swiper-slide').find('video').each(function() {
						this.pause();
					});
	
				},
				on: {
					slideChange: function (el) {
						$('.swiper-slide').each(function () {
							var youtubePlayer = $(this).find('iframe').get(0);
							var video = $(this).find('video').get(0);
							if (youtubePlayer) {
								youtubePlayer.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
							}
							if (video) {
								video.pause();
								$('.video-wrap').removeClass('playing');
							}
						});
					},
				}
	      	});
			  
			if($('.video-wrap').length > 0){
				var video = $('.video-wrap').find('video');
				$('.video-wrap').on('click', function(e){
					var playBtn = $(this);
					$(this).addClass('playing');
					if($('.playing').has) {
						video[0].play();
					}
				});
				$(document).bind("mouseup touchmove", function(e){
					video[0].pause();
					$('.video-wrap').removeClass('playing');
				});
			}
		}
		
		//신상품 비주얼 슬라이드
		if($(".newar-visual-area .bottom-slide").length > 0) {
			var newArSlideBottom = new Swiper(".newar-visual-area .bottom-slide", {
				centeredSlides: true,
				//initialSlide: 1,
	        	slidesPerView: 1.15,
						watchOverflow:true,
	        	pagination: {
	        		el: ".newar-visual-area .slider-page-bar",
					type: "progressbar"
	        	}
	      	});
	      	
			var newArSlideTop = new Swiper(".newar-visual-area .top-slide", {
				// centeredSlides: true,
				// slidesPerView: 1.15,
				// effect: "fade",
				// fadeEffect: {
					// crossFade: true
				// }
	      	});
	      	newArSlideTop.controller.control = newArSlideBottom;
	    	newArSlideBottom.controller.control = newArSlideTop;
		}
		
		//상품 리스트 슬라이드
		if($(".item-list-slide").length > 0) {
			new Swiper(".item-list-slide .item-list", {
				slidesPerView: "auto",
	        	freeMode: true,
	        	slidesOffsetAfter: 30
			});
		}

		if($(".item-list-slide").length > 0) {
			new Swiper(".event-slide .item-list", {
				slidesPerView: "auto",
	        	pagination: {
	        		el: ".event-slide .slider-page-dot",
							clickable: true,
	        	},
						autoplay : true,
						loop: true,
			});
		}
		
		//카테고리 탭 슬라이드
		if($(".category-tab-slide").length > 0) {
			new Swiper(".category-tab-slide .tab-slide", {
						slidesPerView: "auto",
	        	freeMode: true,
	        	slidesOffsetBefore: 20,
	        	spaceBetween: 5
			});
		}
		
		//리뷰 베스트 슬라이드
		if($(".item-review-slide").length > 0) {
			var newArSlideBottom = new Swiper(".item-review-slide", {
	        	slidesPerView: 1.16,
	        	spaceBetween: 15,
	        	slidesOffsetBefore: 20,
	        	slidesOffsetAfter: 20
	      	});
      	}
      	
      	//리뷰 베스트 슬라이드
      	if($(".review-best-slide-area .review-best-slide").length > 0) {
			var newArSlideBottom = new Swiper(".review-best-slide-area .review-best-slide", {
	        	slidesPerView: 1,
						watchOverflow:true,
	        	pagination: {
	        		el: ".review-best-slide-area .slider-page-dot"
	        	},
	      	});
		}
		
		
		//메인비주얼배너 팝업
		$('.category-visual-area .btn-detail-popup').on('click', function(){
			NR.popupOpen($("#popup-visual"));
			return false;
		});
		
		//브랜드 비주얼 슬라이드
		if($(".brand-slide-area .brand-slide").length > 0) {
			var newArSlideBottom = new Swiper(".brand-slide-area .brand-slide", {
				centeredSlides: true,
				//initialSlide: 1,
	        	slidesPerView: 1.25,
	        	spaceBetween: 10,
	        	loop: true,
	        	loopedSlides: 2,
						watchOverflow:true,
	        	pagination: {
	        		el: ".brand-slide-area .slider-page-bar",
					type: "progressbar"
	        	}
	      	});
      	}
      	
      	//앱시작 팝업 슬라이드
		if($("#popup-app-start .app-start-slide").length > 0) {
			var appStartCount = $("#popup-app-start .swiper-slide").length;
			if(appStartCount <= 1) {
				return;
			}
			var appStartSlide = new Swiper("#popup-app-start .app-start-slide", {
				autoplay: true,
				loop: true,
	        	slidesPerView: 1
	      	});
	      	$("#popup-app-start .slide-count").html("1 / " + appStartCount);
	      	appStartSlide.on("slideChange", function() {
	      		$("#popup-app-start .slide-count").html((this.realIndex + 1) + " / " + appStartCount);
	      	});
	      	
      	}
	},

	accordion: function (){
		$(".accordion-list .accordion-title.show").removeClass("show").addClass("active").next(".accordion-content").show();
		$(".accordion-list .accordion-title").on("click", function() {
			if($(this).hasClass("active")) {
				$(this).removeClass("active").next(".accordion-content").slideUp(200);
			}
			else {
				$(".accordion-list .accordion-title.active").removeClass("active").next(".accordion-content").slideUp(200);
				$(this).addClass("active").next(".accordion-content").slideDown(200);
			}
			return false;
		});
	},
	
	selectbox: function() {
		//TODO 동작 확인을 위한 임시 스크립트
		$(".select-box[data-target]").on("change", function() {
			var target = $(this).data("target");
			$(target).removeClass("select-hide");
		});
	},
	selectDirect: function(){
		if( $('.form-email').length > 0 ){
			$('.form-email .select-box').change(function(e){
				if( $(this).val() === "dir" ){
					$(this).next('.dir-wrap').show();
					$(this).hide();
				}
				$('.dir-wrap .close-dir-input').click(function(e){
					e.preventDefault();
					$(this).parents('.dir-wrap').hide();
					$(this).parents('.dir-wrap').prev('.select-box').show().val("");
				})
				
			})
		}
	},

	discountToggle: function(){
		if( $('.payment-table .sale-detail').length > 0 ){
			$('.payment-table .btn-detail-popup').click(function(e){
				var trg = $(this).data('target');
				e.preventDefault();
				$(this).toggleClass('active');
				$(trg).toggleClass('active');
			})
		}
	},

	deliveryMessage: function(){
		if( $('.delivery-message').length > 0 ){
			$('.delivery-message').change(function(e){
				var selected = $(this).val();
				if ( selected == 'typeSelf'){
					$(this).next('.message-wrap').show();
				} else {
					$(this).next('.message-wrap').hide();
				}
			})
		} 
	},
	
	home: function () {
		if($(".home-content-wrap").length == 0) return;
		
		//home 카테고리 메뉴 슬라이드
		if($(".home-category-menu").length > 0) {
			var swiper = new Swiper(".home-category-menu .category-menu", {
				slidesPerView: "auto",
		        freeMode: true,
		        slidesOffsetBefore: 20,
		        slidesOffsetAfter: 20,
		        spaceBetween: 15
	      	});
		}
		
		//item gnb 슬라이드
		if($(".item-gnb .gnb-slide").length > 0) {
			var swiper = new Swiper(".item-gnb .gnb-slide", {
				slidesPerView: "auto",
		        freeMode: true,
		        slidesOffsetBefore: 12,
		        slidesOffsetAfter: 12
	      	});
		}
		
		//브랜드 슬라이드
		if($(".brandline-slide-area .brandline-slide").length > 0) {
			var newArSlideBottom = new Swiper(".brandline-slide-area .brandline-slide", {
	        	slidesPerView: 1,
	        	spaceBetween: 10,
						watchOverflow:true,
	        	pagination: {
	        		el: ".brandline-slide-area .slider-page-bar",
					type: "progressbar"
	        	}
	      	});
		}
		
		//베스트 포토 리뷰 슬라이드
		if($(".review-slide-area").length > 0) {
			var swiper = new Swiper(".review-slide-area .review-slide", {
				slidesPerView: "auto",
				slidesOffsetBefore: 20,
				slidesOffsetAfter: 20,
				spaceBetween: 20,
				on: {
					/* 마지막 slide를 active처리하기 위한코드임: 우측여백이 생기므로 제거함
					reachEnd: function() {
						this.snapGrid = [...this.slidesGrid];
						setTimeout(() => {
							document.querySelector('.swiper-button-next').click()
							clearTimeout()
						}, 1);
					},
					*/
				}
			});
		}
		
		//타임특가 슬라이드
		if($(".timespecial-slide").length > 0) {
			var timespecialThumb = $(".home-time-special .time-thumb .btn-time").clone();
			var timespecialSlide = new Swiper(".timespecial-slide", {
	        	slidesPerView: 1,
	        	effect: "fade",
				fadeEffect: {
					crossFade: true
				},
				watchOverflow:true,
	        	pagination: {
	        		el: ".home-time-special .time-thumb",
	        		clickable: true,
	        		bulletClass: "btn-time",
	        		bulletActiveClass: "active",
	        		renderBullet: function (index) {
						return timespecialThumb.eq(index)[0].outerHTML;
				    }
	        	}
	      	});

			var firstDday = $('.due-date').attr('data-firdt');
			var dday = $('.due-date').attr('data-dt');
			var bgWidth = (100 - (dday/firstDday) * 100) + "%" ; 

			$(window).on("scroll",function(){
				var timePosition = $('.home-time-special').offset().top - $(window).height() * 0.6 ;
				if($(window).scrollTop() > timePosition ) {
					$('.time-box').addClass('active');
					$('.time-box.active .time-bg').css({'width' : bgWidth},1000);
				}
			});
		}
		
		//gnb 슬라이드 탭
		$("#gnb-hot, #gnb-rcmd").on("click", ".btn-menu", function() {
			var getTarget = $(this).data("target");
			var getIdx = $(this).data("idx");
			$(this).closest(".gnb-slide").find(".btn-menu").removeClass("active").eq(getIdx).addClass("active");
			$(getTarget).find(".gnb-slide-tab").removeClass("active").eq(getIdx).addClass("active");
			return false;
		});
		
		//랭킹 베스트 탭
		$("#ranking-tab .btn-tab").on("click", function() {
			$("#ranking-tab .btn-tab").removeClass("active");
			var getIdx = $(this).addClass("active").index();
			$(".ranking-list").removeClass("active").eq(getIdx).addClass("active");
			return false;
		});
	},
	
	cart: function() {
		//구매하기
		$("#popup-item-option .icon-option-close").on("click", function() {
			NR.popupClose($(this).closest(".popup-window"));
			return false;
		});
		
		//장바구니
		$(".btn-item-wish").on("click", function() {
			$(this).toggleClass("active");
			return false;
		});
		
		//수량 
		$(".count-box").each(function() {
			if($(this).hasClass("disabled") || $(this).closest(".item-box").hasClass("disabled")) return true;
			var getCount = $(this).find(".count");
			var getValue = $(this).find(".value");
			var getMax = $(this).data("max");
			var getMin = $(this).data("min");
			$(this).find(".btn-count").on("click", function() {
				var value = getCount.text();
				if(value == undefined || value == "" || isNaN(value)) value = 1;
				value = parseInt(value);
				if($(this).hasClass("btn-down")) {
					value -= 1;
					if(getMin !== undefined && !isNaN(getMin) && getMin > value) value = getMin;
				}
				else if($(this).hasClass("btn-up")) {
					value += 1;
					if(getMax !== undefined && !isNaN(getMax) && getMax < value) value = getMax;
				}
				if(value < 1) value = 1;
				getCount.html(value);
				getValue.html(value);
				return false;
			});
		});
		
		//상품상세 더보기
		$(".pd-content-detail .btn-content-more").on("click", function() {
			$(".pd-content-detail").addClass("more");
			return false;
		});
	},
	
	map: function() {
		if($("#map").length == 0) return;
		
		var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
		var options = { //지도를 생성할 때 필요한 기본 옵션
			center: new kakao.maps.LatLng(37.5080032,127.0605807), //지도의 중심좌표.
			level: 5 //지도의 레벨(확대, 축소 정도)
		};
		
		var map = new kakao.maps.Map(container, options); //지도 생성 및 객체 리턴
		var markerPosition  = new kakao.maps.LatLng(37.5080032,127.0605807);
		//var markerImage = new kakao.maps.MarkerImage("http://nature.afreehp.kr/src/img/icons/icon_map_marker.png", new kakao.maps.Size(51, 76));
		
		var imageSrc = "http://nature.afreehp.kr/src/img/icons/icon_map_marker.png";    
	    var imageSize = new kakao.maps.Size(35, 47);
		      
		var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize);
		
		var mapTypeControl = new kakao.maps.MapTypeControl();
		map.addControl(mapTypeControl, kakao.maps.ControlPosition.TOPRIGHT);

		var zoomControl = new kakao.maps.ZoomControl();
		map.addControl(zoomControl, kakao.maps.ControlPosition.RIGHT);
		
		var marker = new kakao.maps.Marker({
			map: map,
			image: markerImage,
		    position: markerPosition
		});
	},

	skintype: function(){
		if( $('.mypage-skininfo').length > 0 ){
			$('.mypage-skininfo ul li a').on('click', function(e){
				e.preventDefault();
				$(this).toggleClass('active');
			})
		}
	},

	selectPayment: function(){
		if( $('.payment-list').length > 0 ){
			var payments = $('.payment-list a');
			payments.on('click', function(e){
				e.preventDefault();
				payments.removeClass('active');
				$(this).addClass('active');
				var target = $(this).data('payment');
				$('.payment-description > ul > li').hide();
				$('.payment-description').find('.'+target).show();
			})
		}
	},
	
	brand: function() {
		if($(".brand-video .video").length > 0) {
			var brandVideo = $(".brand-video video").get(0);
			var BtnBrandPlay = $(".brand-video .btn-play").on("click", function() {
				brandVideo.play();
				return false;
			});
			$(".brand-video video").on("click", function() {
				brandVideo.pause();
			});
			$(".brand-video .video").on("play", function() {
				BtnBrandPlay.addClass("hide");
			}).on("pause", function() {
				BtnBrandPlay.removeClass("hide");
			}).on("ended", function() {
				BtnBrandPlay.removeClass("hide");
			});
		}
	},
	
	tooltip: function() {
		//툴팁
		$(".btn-tooltip-msg[data-tooltip]").each(function() {
			$(this).append('<div class="tooltip-msg-popup"><p>' + $(this).data("tooltip") + '</p></div>');
		}).on("click", function() {
			$(this).toggleClass("active");
			return false;
		});
		$("body").on("click.tooltip", function(e) {
			if($(e.target).closest(".btn-tooltip-msg").length == 0) {
				$(".btn-tooltip-msg.active").removeClass("active");
			}
		});
	},
	
	materialBox: function() {
		if($(".material-box").length > 0) {
			$(".material-box .btn-round").on("click", function() {
				$(this).parent('.material-box').toggleClass("show");
				return false;
			});			
		}

	},

	daterangepicker: function(){
		if( $('input[name="datepicker"]').length > 0){
			$('input[name="datepicker"]').daterangepicker({
				opens: 'left',
				locale: {
					"applyLabel": "적용",
					"cancelLabel": "취소",
					"weekLabel": "주",
					// "format": "MM/DD/YYYY",
					// "separator": " - ",
					// "fromLabel": "From",
					// "toLabel": "To",
					// "customRangeLabel": "Custom",
					"daysOfWeek": [
							"일",
							"월",
							"화",
							"수",
							"목",
							"금",
							"토"
					],
					"monthNames": [
							"1월",
							"2월",
							"3월",
							"4월",
							"5월",
							"6월",
							"7월",
							"8월",
							"9월",
							"10월",
							"11월",
							"12월"
					],
					"firstDay": 1
				}
			});
			
		}
		if( $(".date-select").length > 0 ) {
			$('.date-select').change(function(e){
				var selected = $(this).val();
				if ( selected == 'typeSelf'){
					$(this).next('.datepicker-wrap').show();
				} else {
					$(this).next('.message-wrap').hide();
				}
			})
		}
	},

}

function init(){
	Object.keys(NR).forEach(function(fn){
		NR[fn]();
	});
}

$('documnet').ready(function(){
	init();
});


function TimeDiscountHour(setTime){
	var set = setTime;
	// 인증시간 카운트다운
	function timeTemplate(sec){
		var remainTime = sec / 1000;

		var time = "";
		var hour = Math.floor( remainTime / 3600 );
		remainTime -= 3600 * hour;
		if( hour < 10 ){
			hour = '0' + hour;
		}
		var minute = Math.floor( remainTime / 60 );
		remainTime -= 60 * minute;
		if( minute < 10 ){
			minute = '0' + minute;
		}
		var seconds = Math.floor( remainTime % 60 );
		if( seconds < 10 ){
			seconds = '0' + seconds;
		}

		var template_hour = '<span class="hour">'+ hour +'</span>:';
		var template_min = '<span class="minute">'+ minute +'</span>:';
		var template_sec = '<span class="second">'+ seconds +'</span>';

		time = template_hour + template_min + template_sec;
		return time
	}
	var interval = setInterval(function(){
		if( set < 1 ){
			clearInterval(interval);
		}                                                           
		var result = timeTemplate(set);
		$('#home-time-special').html(result);
		set -= 1000; 
	}, 1000);
}

function exchangeTypeSelect(list){

	var tpl = "";
	$(list).each(function(i, e){
		var option = "";
		//console.log(e.exchangeType);
		option = "<option value='' data-type='" + e.exchangeType + "' data-notice='" + e.notice + "'>" + e.exchangeType +"</option>";
		tpl += option;
	})
	
	$('.select-box.exchange-type').append(tpl).on('change',function(e){
		var whoPays = $(this).find('option:selected').data('notice');
		var noticeTpl = "반품배송비 : "+ whoPays;
		$(this).next('.refund-desc').html(noticeTpl);

	});
}
